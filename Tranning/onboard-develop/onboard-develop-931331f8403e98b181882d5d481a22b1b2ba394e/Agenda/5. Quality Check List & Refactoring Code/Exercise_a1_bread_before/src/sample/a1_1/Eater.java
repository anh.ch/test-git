package sample.a1_1;

public class Eater {
	public static void main(String[] args) {
		Eater eater = new Eater();
		System.out.println(eater.eatCheeseBread());
		System.out.println(eater.eatCheeseAndSesameBread());
		System.out.println(eater.eatCheeseAndSesameAndBaconBread());
	}

	public String eatCheeseBread() {
		String source1 = "Cheese";
		return eat(new Bread(source1));
	}

	public String eatCheeseAndSesameBread() {
		String source1 = "Cheese";
		String source2 = "Sesame";
		return eat(new Bread(source1, source2));
	}

	public String eatCheeseAndSesameAndBaconBread() {
		String source1 = "Cheese";
		String source2 = "Sesame";
		String source3 = "Bacon";
		return eat(new Bread(source1, source2, source3));
	}

	private String eat(Bread bread) {
		return "I ate " + bread.bake() + ".";
	}
}
