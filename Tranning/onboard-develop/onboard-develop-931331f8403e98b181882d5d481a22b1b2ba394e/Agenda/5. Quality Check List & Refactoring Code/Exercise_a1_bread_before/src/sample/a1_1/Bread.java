package sample.a1_1;

import java.util.ArrayList;

public class Bread {
	private String breadName; // bread name
	private ArrayList fillings;

	// constructor for one source
	public Bread(String source1) {
		fillings = new ArrayList();
		if (null != source1) {
			fillings.add(source1);

		}

	}

	// constructor for two sources
	public Bread(String source1, String source2) {
		fillings = new ArrayList();
		if (null != source1) {
			fillings.add(source1);
		}
		if (null != source2) {
			fillings.add(source2);

		}
	}

	// constructor for three sources
	public Bread(String source1, String source2, String source3) {
		fillings = new ArrayList();
		if (null != source1) {
			fillings.add(source1);
		}
		if (null != source2) {
			fillings.add(source2);
		}
		if (null != source3) {
			fillings.add(source3);
		}
	}

	public String bake() {
		breadName = "Unknown Bread";
		switch (fillings.size()) {
		case 1:
			if ("Cheese" == ((String) fillings.get(0))) {
				breadName = "CheeseBread";
			}
			break;
		case 2:
			if ("Cheese" == ((String) fillings.get(0))
					&& "Sesame" == ((String) fillings.get(1))) {
				breadName = "CheeseAndSesameBread";
			}
			break;
		case 3:
			if ("Cheese" == ((String) fillings.get(0))
					&& "Sesame" == ((String) fillings.get(1))
					&& "Bacon" == ((String) fillings.get(2))) {
				breadName = "CheeseAndSesameAndBaconBread";
			}
			break;
		}
		return breadName;
	}

	public String getBreadName() {
		return breadName;
	}

	public void setBreadName(String breadName) {
		this.breadName = breadName;
	}
}
