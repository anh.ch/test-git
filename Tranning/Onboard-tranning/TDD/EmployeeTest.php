<?php
   require 'Employee.php';
/**
 * Created by PhpStorm.
 * User: anh.ch
 * Date: 10/6/2016
 * Time: 9:41 PM
 */

class EmployeeTest extends PHPUnit_Framework_TestCase
{
    private $employee;

    protected function setup(){
        return $this->employee = new Employee();
    }
    protected function teardown(){
        $this->employee = null;
    }
    //TH1: nhập vào là số ngày âm;
    public function TestInputDayIsNegativeNumber(){
        //given
            $day = -22;
            $err= 0;
        //when
            $isErr = $this->employee->InputDayIsNegativeNumber($day);
        //then
            $this->assertEquals($err,$isErr);
    }
    //Th2: nhập vào là "";
    public function TestInPutDayIsEmpty(){
        $day ="";
        $err = false;

        $isErr =$this->employee->InputDayIsEmpty($day);

        $this->assertEquals($err,$isErr);
    }
    //Th3: đi làm theo ngày


    public function testSalary(){
        //given
            $days = 22;
            $wage = 22;
        //when
        $salary = $this->employee->Salary($days,$wage);
        //then
        $this->assertEquals(22,$salary);
    }
}
